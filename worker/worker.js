const kue           = require('kue');
const axios         = require('axios');
const Raven         = require('raven');

const config        = require('../config/config');

Raven.config(config.ravenWorker).install();

const queue = kue.createQueue({
    "prefix": "q","postback": {
      "host": "127.0.0.1"
    }
});

//
// ─── PROCESS QUEUE ──────────────────────────────────────────────────────────────
//
async function sendPostback(link, done) {
    try {
        await axios.get(link);
        done();
    } catch ( e ) {
        Raven.captureException(e);
        done(new Error(`error at sending ${e}`))
    }
}

queue.process('postback', (job, done) => {
    sendPostback(job.data.to, done);
});
