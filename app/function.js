//
// ─── CONSTANT DEFENITION ────────────────────────────────────────────────────────
//

const REQ_COUNT = 30;
const DAY       = 7;
const STATUS_INIT = 2;
const STATUS_GOOD = 1;
const STATUS_BAD = 3;

//
// ─── FUNCTIONS DEFENITION ────────────────────────────────────────────────────────
//

module.exports.parseCookies = function ( rc ) {

    let list = {};

    rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;

}

module.exports.getCountryCode = function ( request ) {

    let rt          = {};

    const ip        = request.headers['x-real-ip'];
    const geo       = geoip.lookup(ip);

    if ( typeof geo != 'undefined' && geo && geo.country) {
        const countryCode   = geo.country;
        return rt = { code:countryCode, ip: ip };
    }
    return rt = { code:'unresolved', ip: ip };

}

module.exports.cnd = function () {
    
}

module.exports.checkCookie = async function ( cj, client, queue, res ) {
    if ( cj.hasOwnProperty('cpaelectro') ){
        const name      = 'cpaelectro';
        const value     = cookieJar['cpaelectro'];

        // get postback urls
        const postback  = await client.hgetAsync('partners', name); 

        const flag      = await client.hsetnxAsync(`postback-integration:${value}`, 'installed', 'true');

        if ( flag == 1 ) { 

            // send postback with status 2
            queue.create('postback', {
                to: `${postback}${value}&status=${STATUS_INIT}`
            })
            .removeOnComplete(true)
            .save()

            await client.hsetAsync(`postback-integration:${value}`, 'dateInstalled', new Date());

        }  else {

            // set visited count + 1
            await client.hincrbyAsync(`postback-integration:${value}`, 'visited_count', 1);
            // get visited_count
            const visitedCount  = await client.hgetAsync(`postback-integration:${value}`, 'visited_count'); // it's not good
            const date          = await client.hgetAsync(`postback-integration:${value}`, 'dateInstalled');
            
            const diffDays      = Math.round(Math.abs((new Date().getTime() - new Date(date).getTime())/(1000*60*60*24)));
        

            if ( (diffDays <= DAY) && (visitedCount >= REQ_COUNT) ) {
                
                queue.create('postback', {
                    to: `${postback}${value}&status=${STATUS_GOOD}`
                })
                .removeOnComplete(true)
                .save()
                
                //set status and date when posback has been send
                await client.hmsetAsync(`postback-integration:${value}`, ['dateRemoved', new Date(), 'status', STATUS_GOOD]);

                res.setHeader(
                    'Set-Cookie',`${name}=${value};path='/';expires=`+new Date(1)
                );
                
            } else if ( diffDays > DAY ) {
                
                queue.create('postback', {
                    to: `${postback}${value}&status=${STATUS_BAD}`
                })
                .removeOnComplete(true)
                .save()

                //set status and date when posback has been send
                await client.hmsetAsync(`postback-integration:${value}`, ['dateRemoved', new Date(), 'status', STATUS_BAD]);

                res.setHeader(
                    'Set-Cookie',`${name}=${value};path='/';expires=`+new Date(1)
                );
                
            }
        }
    } else if ( cj.hasOwnProperty('kadam') ) {
        const name      = 'kadam';
        const value     = cookieJar['kadam'];

        const postback  = await client.hgetAsync('partners', name); 
        
        const flag      = await client.hsetnxAsync(`postback-integration:${value}`, 'installed', 'true');

        if ( flag == 1 ) {

            queue.create('postback', {
                to: `${postback}${value}&status=hold`
            })
            .removeOnComplete(true)
            .save()

            await client.hsetAsync(`postback-integration:${value}`, 'dateInstalled', new Date());

        } else {
            // set visited count + 1
            await client.hincrbyAsync(`postback-integration:${value}`, 'visited_count', 1);
            // get visited_count
            const visitedCount  = await client.hgetAsync(`postback-integration:${value}`, 'visited_count'); // it's not good

            if ( visitedCount > 10 ){

                queue.create('postback', {
                    to: `${postback}${value}&status=approved`
                })
                .removeOnComplete(true)
                .save()
                
                //set status and date when posback has been send
                await client.hmsetAsync(`postback-integration:${value}`, ['dateRemoved', new Date(), 'status', STATUS_GOOD]);

                res.setHeader(
                    'Set-Cookie',`${name}=${value};path='/';expires=`+new Date(1)
                );
                
            }
        }
    }
    else {}
}