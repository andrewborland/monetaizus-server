const http          = require('http');
const url           = require('url');
const Promise       = require('bluebird');
const MongoClient   = Promise.promisifyAll(require('mongodb').MongoClient);
const redis         = require('redis');
const pino          = require('pino')();
const kue           = require('kue');
const Raven         = require('raven');

Promise.promisifyAll(redis.RedisClient.prototype);

const config        = require('./config/config');
const fnk           = require('./app/function.js');

Raven.config(config.ravenServer).install();

let _client;
let _db;

//
// ─── CREATE QUEUE ───────────────────────────────────────────────────────────────
//

const queue = kue.createQueue({
    "prefix": "q","postback": {
      "host": "127.0.0.1"
    }
});

//
// ─── REQUEST HANDLER ────────────────────────────────────────────────────────────
//

async function reqHandler (req, res) {

    try {

        const data = url.parse(req.url, true).query;

        // set cookie from lending page
        if ( data.partner && data.click ) {

            // set cookie
            res.setHeader(
                'Set-Cookie', [`${data.partner}=${data.click};path='/';expires=`+new Date(new Date().getTime()+86409000*360).toUTCString(), `m_sid=${data.click};path='/';expires=`+new Date(253402300000000).toUTCString()]
            );
            return res.end(); 

        } 

        // proccess request from extension
        if ( data.id && data.location ) {

            // collect stats
            await _client.hincrbyAsync('stats', 'monetaizus-server', 1);
            await _client.hincrbyAsync('urls', data.location, 1);
            
            // resolve monetaizus
            const collectionExt     = _db.collection('extensions');
            const response          = await collectionExt.findOne({id:data.id});
            if ( !response ) return res.end();
            const monetaizusUrl     = response.monetaizusUrl;
            if ( !monetaizusUrl ) return res.end();
            
            // if we get cookie with request
            if ( req.headers.cookie ) {

                const cookieJar = fnk.parseCookies(req.headers.cookie);

                await fnk.checkCookie( cookieJar, _client, queue, res );

            }

            res.writeHead(302, {
                'Location': `${monetaizusUrl}?sid=`
            });

            return res.end();
            
        }
    
        res.end();
    
    } catch ( err ) { 

        Raven.captureException(err);
        res.end();
    
    }

}
//
// ─── START SERVER ───────────────────────────────────────────────────────────────
//

const server = http.createServer(reqHandler);

MongoClient.connect(`${config.mongo.ip}/${config.mongo.name}`)
    .then( (client) => {
        _db = client.db(config.mongo.name);
    })
    .then( () => {
        _client = redis.createClient({host:config.redis.host, port:config.redis.port});
    })
    .then( () => {
        server.listen(config.port);
    })
    .catch( ( err ) => {
        Raven.captureException(err);
    })
